CIRCLE = "O"
CROSS = "X"

tic_tac_toe = [
       "[0]", "[1]", "[2]", "\n",
       "[4]", "[5]", "[6]", "\n",
       "[8]", "[9]", "[10]"
]

def print_my_board():
    print("".join(tic_tac_toe))

print_my_board()

my_occupied_position_list = []


def place_mark_in_board(position, x):
    my_occupied_position_list.append(position)
    tic_tac_toe[position] = x

place_mark_in_board(5, CIRCLE)
place_mark_in_board(2, CROSS)

print('---')
print_my_board()
print('---')
print(my_occupied_position_list)

winning_options = [0,1,2]